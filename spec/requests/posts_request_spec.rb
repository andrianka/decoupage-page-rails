# frozen_string_literal: true

RSpec.describe 'Posts', type: :request do
  let(:user) { create(:user) }
  let(:another_user) { create(:another_user) }
  let(:user_post) { create(:post, user: user) }
  let(:another_user_post) { create(:post, user: another_user) }


  context 'abilities as guest' do
    subject(:ability) { Ability.new(nil) }
    it { is_expected.to_not be_able_to(:create, Post.new) }
    it { is_expected.to_not be_able_to(:index, Post) }
    it { is_expected.to_not be_able_to(:show, user_post) }
    it { is_expected.to_not be_able_to(:destroy, user_post) }
    it { is_expected.to_not be_able_to(:update, user_post) }
    it { is_expected.to_not be_able_to(:edit, user_post) }
  end

  context 'abilities as logged user' do
    before(:each) { sign_in user }
    subject(:ability) { Ability.new(user) }
    it { is_expected.to be_able_to(:manage, user_post) }
    it { is_expected.to_not be_able_to(:manage, another_user_post) }
  end

  context 'requests as logged user' do
    before(:each) { sign_in user }

    describe '#index' do
      it 'return :ok' do
        get posts_path
        expect(response).to have_http_status(:success)
      end
    end

    describe '#show' do
      it 'return :success' do
        get post_path(user_post)
        expect(response).to have_http_status(:success)
      end
    end

    describe '#edit' do
      it 'return :success' do
        get edit_post_path(user_post)
        expect(response).to have_http_status(:success)
      end
    end

    describe '#create' do
      it 'get successfully notice' do
        post posts_path, params: { post: { content: 'fff' } }
        follow_redirect!
        expect(response.body).to include('Post was successfully created.')
      end
      it 'get error msg' do
        post posts_path, params: { post: { content: '' } }
        expect(response.body).to include('can&#39;t be blank')
      end
    end

    describe '#update' do
      it 'get successfully notice' do
        put post_path(user_post), params: { post: { content: 'updated' } }
        follow_redirect!
        expect(response.body).to include('Post was successfully updated.')
        expect(response).to have_http_status(:ok)
      end
      it 'get error msg' do
        put post_path(user_post), params: { post: { content: '' } }
        expect(response.body).to include('can&#39;t be blank')
      end
    end

    describe '#destroy' do
      it 'successfully delete' do
        delete post_path(user_post.id)
        follow_redirect!
        expect(Post.find_by(id: user_post.id)).to be_nil
        expect(response.body).to include('Post was successfully destroyed.')
      end
      it 'raises an error when the id is not valid' do
        expect { delete post_path('unknown') }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  context 'requests as guest' do
    describe '#index' do
      it 'render login page' do
        get posts_path
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
    describe '#edit' do
      it 'render login page' do
        get edit_post_path(user_post)
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
    describe '#show' do
      it 'render login page' do
        get post_path(user_post)
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
    describe '#post' do
      it 'render login page' do
        post posts_path(), params: { post: { content: 'updated' } }
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
    describe '#update' do
      it 'render login page' do
        put post_path(user_post), params: { post: { content: 'updated' } }
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
    describe '#destroy' do
      it 'render login page' do
        delete post_path(user_post)
        follow_redirect!
        expect(response.body).to include('Please Login to view that page!')
      end
    end
  end
end
