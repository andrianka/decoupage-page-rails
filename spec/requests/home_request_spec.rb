# frozen_string_literal: true

RSpec.describe 'Home', type: :request do
  let(:user) { create(:user) }

  context 'requests as guest' do
    subject(:ability) { Ability.new() }
  end

  context 'requests as logged user' do
    before(:each) { sign_in user }
    subject(:ability) { Ability.new(user) }

    describe '#index' do
      before(:each) { get root_path }
      it 'return :ok' do
        expect(response).to have_http_status(:success)
      end

      it 'render editor' do
        expect(response.body).to include('Create your content to print')
      end
    end
  end
end
