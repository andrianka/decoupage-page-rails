# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:post) { build(:post) }

  describe 'validation' do
    it 'is valid with valid data' do
      expect(post).to be_valid
    end

    it 'is invalid without user' do
      post.user = nil
      expect(post).to_not be_valid
    end

    it 'is invalid without content' do
      post.content = nil
      expect(post).to_not be_valid
    end

    it 'is invalid with empty content' do
      post.content = ''
      expect(post).to_not be_valid
    end
  end
end
