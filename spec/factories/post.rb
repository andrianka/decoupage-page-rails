# frozen_string_literal: true

require 'faker'
FactoryBot.define do
  factory :post do
    content { Faker::Lorem.paragraph }
    user
  end

  factory :post2 do
    content { Faker::Lorem.paragraph }
    user2
  end
end
