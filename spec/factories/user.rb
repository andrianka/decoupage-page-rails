# frozen_string_literal: true

require 'faker'
FactoryBot.define do
  password = Faker::Internet.password
  factory :user do
    email { Faker::Internet.email }
    password { password }
    password_confirmation { password }
  end

  password_user2 = Faker::Internet.password
  factory :another_user, class: User do
    email { Faker::Internet.email }
    password { password_user2 }
    password_confirmation { password_user2 }
  end
end
