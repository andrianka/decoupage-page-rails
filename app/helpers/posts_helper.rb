# frozen_string_literal: true

module PostsHelper
  def form_title(post)
    post.new_record? ? 'Create your content to print' : 'Edit your post'
  end

  def post_thumb_content(post)
    truncate(strip_tags(post.editor_content.to_s), length: 100)
  end

  def post_content(post)
    post.editor_content.try(:html_safe)
  end

  def post_time_created(post)
    time_ago_in_words(post.created_at)
  end
end
