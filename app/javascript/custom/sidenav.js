document.addEventListener('turbolinks:load', function(event){
  /* Set the width of the side navigation to 250px */
  window.openNav = function() {
    document.getElementById("mySidenav").style.width = "250px";
  };

  /* Set the width of the side navigation to 0 */
  window.closeNav = function() {
    document.getElementById("mySidenav").style.width = "0";
  };
});
