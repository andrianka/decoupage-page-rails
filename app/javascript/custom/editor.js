document.addEventListener('turbolinks:load', function(event){
  if (tinyMCE.activeEditor != null) {
    tinyMCE.activeEditor.destroy();
  };
  tinyMCE.init({
    selector: "#editor",
    height: 500,
    plugins: 'print preview paste image imagetools lists',
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | insertfile image media | reverseText' ,
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*')
      input.onchange = function () {
        var file = this.files[0];

        var reader = new FileReader();
        reader.onload = function () {
          var id = 'blobid' + (new Date()).getTime();
          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);

          /* call the callback and populate the Title field with the file name */
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      }
      input.click();
    },
    toolbar_sticky: true,
    autosave_ask_before_unload: true,
    autosave_interval: "30s",
    autosave_retention: "2m",
    autosave_prefix: "{path}{query}-{id}-",
    autosave_restore_when_empty: false,
    image_advtab: true,
    paste_retain_style_properties: "all",
    formats: {
      customformat: {block:"p", styles: { transform: "scaleX(-1)" }, attributes: { title: 'Reverse text'}}
    },
    style_formats: [
      {title: 'Reverse Text', format: 'customformat'},
    ],


    setup: (editor) => {
      editor.ui.registry.addToggleButton('reverseText', {
        text: "Reverse Text",
        onAction: () =>{
          tinyMCE.activeEditor.formatter.apply('customformat');
        }
      })
    }
  });
});
