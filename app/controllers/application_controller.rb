# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  private
    def authenticated_user!
      session['user_return_to'] = request.fullpath
      if user_signed_in?
        authenticate_user!
      else
        redirect_to new_user_session_path, notice: 'Please Login to view that page!'
      end
    end

    # Deletes the return path as soon as it's used, so
    # they aren't accidentally redirected back again
    # next time they login

    def after_sign_in_path_for(resource)
      session.delete('user_return_to') || root_path
    end
end
