# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :authenticated_user!
  load_and_authorize_resource
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @posts = Posts::PostManage.new(current_user, params[:search]).get_posts
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      flash[:notice] = 'Post was successfully created.'
      redirect_to @post
    else
      flash[:error] = @post.errors.full_messages
      render 'home/index'
    end
  end

  def update
    if @post.update(post_params)
      flash[:notice] = 'Post was successfully updated.'
      redirect_to @post
    else
      flash[:error] = @post.errors.full_messages
      render :edit
    end
  end

  def destroy
    if @post.destroy
      flash[:notice] = 'Post was successfully destroyed.'
      redirect_to posts_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = current_user.posts.find(params[:id])
    end
    # Only allow a list of trusted parameters through.
    def post_params
      params.fetch(:post, {}).permit(:title, :content)
    end
end
