# frozen_string_literal: true

class Posts::PostManage
  def initialize(user, params)
    @user = user
    @params = params
  end

  def get_posts
    return @user.posts if @params.nil?
    Post.search(@params)
  end
end
