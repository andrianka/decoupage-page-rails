# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if !user.new_record?
      can :manage, Post, { user_id: user.id }
      can :create, Post
    end
  end
end
