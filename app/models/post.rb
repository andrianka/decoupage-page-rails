# frozen_string_literal: true

class Post < ApplicationRecord
  searchkick
  belongs_to :user

  has_rich_text :content

  validates_presence_of :content, :user

  def search_data
    {
      title: title,
      content: content
    }
  end

  def editor_content
    self.try(:content).try(:body)
  end
end
